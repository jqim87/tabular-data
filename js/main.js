function form() {
    var $form = $('#addDataForm');
    var $name = $form.find("input[name='nameInput']");
    var $dob = $form.find("input[name='dobInput']");
    var $email = $form.find("input[name='emailInput']");
    var $children = $form.find("input[name='childrenInput']");
    var $btn = $form.find("button[name='addData']");
    var isFormValid = true;

    var handelClick = function() {

        if (isFormValid == true) {
            var formData = {
                name: $name.val(),
                dob: $dob.val(),
                email: $email.val(),
                children: $children.val()
            };
            putDataSQLl(formData);
            clearForm();
            addTableRow(formData);
        } else {
            showValidation('all');
        }

    };

    $btn.on('click', function(){
        handelClick();
    });

    function clearForm() {
        $name.val(''),
        $dob.val(''),
        $email.val(''),
        $children.val('')
    };
};


function validation() {

    $(".form-group").each(function() {
        var $input = $(this).find("input[data-validation]")
        var criteria = $input.data('validation');
        $input.on('blur', function(){
            if ( match($input.val(), criteria) ) {
                $(this).addClass('valid');
            } else {
                $(this).next('.validation-message').addClass('is-visible');
                isFormValid = false;
            }

        });
    });

    function match(value, pattern) {
        console.log('validating input: ' + value + ' against ' + pattern);
        return true;
    }


};

function putDataSQLl(data) {
    console.log('sent this to the server:');
    console.log(data);

};

function addTableRow(data) {
    $("#output-table").append('<tr><td>'+ data.name +'</td><td>' + data.email + '</td></tr>');
    $('.placeholder-row').addClass('is-hidden');
}



(function() {
    console.log('IIFE');
    validation();
    form();
})();
